// UE_HomeworkC++_2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template <typename Type>
class Stack
{
private:
    Type* data;
    int length;
    
public:
    Stack()
    {
        data = nullptr;
        length = 0;
    }
    
    Stack(Type first)
    {
        data = new Type (first);
        length = 1;
    }
    
    void Push(Type el)
    {
        Type* temp = new Type[length+1];
        for (int i = 0; i < length; i++)
            temp[i] = data[i];
        temp[length++] = el;
        if (length > 0)
            delete[] data;
        data = temp;
    }
    Type Pop()
    {
        if (length > 0)
        {
            Type result = Top();
            Type* temp = new Type[--length];
            for (int i = 0; i < length; i++)
                temp[i] = data[i];
            delete[] data;
            data = temp;
            return result;
        }
        else
            return NULL;
    }
    
    Type Top()
    {
        if (length > 0)
            return data[length-1];
        else
            return NULL;
    }
    
    void Clear()
    {
        delete[] data;
        data = nullptr;
        length = 0;
    }
    
    void Show()
    {
        for (int i = 0; i < length; i++)
            std::cout << data[i] << ' ';
        std::cout << std::endl;
    }
};

int main()
{
    Stack<int> intStack;
    intStack.Push(5);
    intStack.Push(10);
    intStack.Push(11);
    intStack.Push(147);
    intStack.Show();
    std::cout << intStack.Pop() << std::endl;
    intStack.Show();
    
    Stack<std::string> stringStack;
    stringStack.Push("elem1");
    stringStack.Push("elem2");
    stringStack.Push("blabla");
    stringStack.Push("skillbox");
    stringStack.Push("last");
    stringStack.Show();
    std::cout << stringStack.Pop() << std::endl;
    stringStack.Show();
    
}
